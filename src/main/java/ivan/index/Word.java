package ivan.index;

public class Word {

    private final String word;
    private final long line;

    public Word(String word, long line) {
        this.word = word;
        this.line = line;
    }

    public String getWord() {
        return word;
    }

    public long getLine() {
        return line;
    }

    @Override
    public String toString() {
        return "Word{" +
                "word='" + word + '\'' +
                ", line=" + line +
                '}';
    }
}
