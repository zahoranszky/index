package ivan.index;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class WordSource implements Iterable<Word> {

    public class WordSourceIterator implements Iterator<Word> {

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public Word next() {
            Word temp = actWord;
            proceed();
            return temp;
        }
    }

    private final BufferedReader reader;
    private Scanner scanner;
    private long actLine = 0;
    private boolean hasNext = true;
    private Word actWord;

    public WordSource(Reader reader) throws IOException {
        this.reader = new BufferedReader(reader);
        readLine();
        proceed();
    }

    private void proceed() {
        if (!hasNext) {
            return;
        }

        if (null != scanner) {
            try {
                actWord = new Word(scanner.next(), actLine);
            }  catch (NoSuchElementException e) {
                readLine();
                proceed();
            }
        }
    }

    private void readLine() {
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (null == line) {
            hasNext = false;
        } else {
            ++actLine;
            scanner = new Scanner(line);
        }
    }

    @Override
    public Iterator<Word> iterator() {
        return new WordSourceIterator();
    }
}
