package ivan.index;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) throws IOException {
        usageIfNeeded(args);

        try {
            checkArguments(args);
            runIt(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private static void checkArguments(String[] args) {
        if (!new File(args[0]).exists()) {
            throw new RuntimeException("Input file does not exist (" + args[0] +")");
        }
    }

    private static void runIt(String[] args) throws IOException {
        WordCounter wc = new WordCounter(args[0]);
        try (PrintWriter pw = new PrintWriter(args[1]);) {
            wc.write(pw);
        }
    }

    private static void usageIfNeeded(String[] args) {
        if (args.length != 2) {
            printUsage();
            System.exit(1);
        }
    }

    private static void printUsage() {
        System.out.println("Usage: java ivan.index.Main <input file> <output file>");
        System.out.println("Example: java ivan.index.Main ./example.txt ./index.txt");
    }

}
