package ivan.index;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.StreamSupport;

public class WordCounter {

    private WordSource wordSource;
    private Map<String, Set<Long>> index = new TreeMap<>();

    public WordCounter(String fileName) throws IOException {
        try (FileReader reader =  new FileReader(fileName)) {
            wordSource = new WordSource(reader);
            process();
        }
    }

    private void process() {
        StreamSupport.stream(wordSource.spliterator(), false).forEach(
            word -> {
                Set<Long> set = index.get(word.getWord());
                if (null == set) {
                    set = new TreeSet<>();
                    index.put(word.getWord(), set);
                }
                set.add(word.getLine());
            }
        );
    }

    public void write(PrintWriter writer) {
        index.entrySet().stream().forEach(entry -> {
            writer.print(entry.getKey());
            writer.print(" ");
            writer.println(set2String(entry.getValue()));
        });
    }

    private String set2String(Set<Long> set) {
        return set.toString().substring(1, set.toString().length() - 1);
    }
}
