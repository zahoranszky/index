package ivan.index;

import static org.junit.Assert.*;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class WordSourceTest {

    @Test
    public void testEmpty() throws IOException {
        WordSource ws = new WordSource(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(
                "".getBytes()))));
        assertFalse(ws.iterator().hasNext());
    }

    @Test
    public void testOne() throws IOException {
        WordSource ws = new WordSource(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(
                "apple".getBytes()))));
        Iterator<Word> it = ws.iterator();
        Word w = it.next();
        assertEquals("apple", w.getWord());
        assertEquals(1, w.getLine());
    }

    @Test
    public void testMore() throws IOException {
        WordSource ws = new WordSource(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(
                "cherry apple\npeach".getBytes()))));
        Iterator<Word> it = ws.iterator();
        Word w = it.next();
        assertEquals("cherry", w.getWord());
        assertEquals(1, w.getLine());
        w = it.next();
        assertEquals("apple", w.getWord());
        assertEquals(1, w.getLine());
        w = it.next();
        assertEquals("peach", w.getWord());
        assertEquals(2, w.getLine());
    }
}
